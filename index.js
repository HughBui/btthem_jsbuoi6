function test(n) {
  var a = true;
  if (n < 2) {
    a = false;
  } else if (n == 2) {
    a = true;
  } else if (n % 2 == 0) {
    a = false;
  } else {
    for (var i = 3; i <= Math.sqrt(n); i += 2) {
      if (n % i == 0) {
        a = false;
        break;
      }
    }
  }
  return a;
}

function ketQua() {
  var number = document.getElementById("number").value;
  number = parseInt(number);
  var result = "";
  for (var i = 1; i <= number; i++) {
    if (test(i)) {
      result += i + " <br/>";
    }
  }
  document.getElementById("result").innerHTML = result;
}
